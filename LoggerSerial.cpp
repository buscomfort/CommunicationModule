#include "LoggerSerial.h"

LoggerSerial::LoggerSerial()
{
	hSerialPort = NULL;
}

void LoggerSerial::Setup(SomeSerial *handler, int baudrate)
{
	hSerialPort = handler;
	hSerialPort->begin(baudrate); /* Inicializando o monitor serial */
}

int LoggerSerial::available(void)
{
	if (IsSet())
		return hSerialPort->available();
	return 0;
}

int LoggerSerial::read(void)
{
	if (IsSet())
		return hSerialPort->read();
	return 0;
}

size_t LoggerSerial::write(byte val)
{
	if (IsSet())
		return hSerialPort->write(val);
	return 0;
}

void LoggerSerial::print(String message)
{
	if (IsSet())
		hSerialPort->print(message);
}

void LoggerSerial::print(double message)
{
	if (IsSet())
		hSerialPort->print(message);
}

void LoggerSerial::print(float message)
{
	if (IsSet())
		hSerialPort->print(message);
}

void LoggerSerial::print(int message)
{
	if (IsSet())
		hSerialPort->print(message);
}

void LoggerSerial::println(String message)
{
	if (IsSet())
		hSerialPort->println(message);
}

void LoggerSerial::println(double message)
{
	if (IsSet())
		hSerialPort->println(message);
}

void LoggerSerial::println(float message)
{
	if (IsSet())
		hSerialPort->println(message);
}

void LoggerSerial::println(int message)
{
	if (IsSet())
		hSerialPort->println(message);
}

bool LoggerSerial::IsSet(void)
{
	if (hSerialPort != NULL)
	{
#if defined(ARDUINO_ARCH_SAM)
		if (hSerialPort->isSerial_())
			return (*hSerialPort->thisSerial_);
#endif
		if (hSerialPort->isHardwareSerial())
			return (*hSerialPort->thisHardwareSerial);
		return true;
	}
	return false;
}