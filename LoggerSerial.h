#ifndef LOGGERSERIAL_H_
#define LOGGERSERIAL_H_

#include <Arduino.h>
#include <SomeSerial.h>

class LoggerSerial
{
public:
	SomeSerial *hSerialPort;
	int baudrate;
	bool enabled;

	LoggerSerial();
	void Setup(SomeSerial *handler, int baudrate);
	int available(void);
	int read(void);
	size_t write(byte val);
	void print(String message);
	void print(double message);
	void print(float message);
	void print(int message);
	void println(String message);
	void println(double message);
	void println(float message);
	void println(int message);

	bool IsSet(void);
};
#endif /* LOGGERSERIAL_H_ */