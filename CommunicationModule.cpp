#include "CommunicationModule.h"

CommunicationModule::CommunicationModule()
{
	loggerSerial == NULL;
	radioEngeLoRaMESH = NULL;
	communicationSuccess = false;
	receivedDataValid = false;
}

void CommunicationModule::Setup(CommunicationModuleType moduleType,
								uint16_t payloadSize, uint32_t ackTimeout, uint8_t numberRetries,
								SomeSerial *commandSerial, uint32_t baudRateCommand,
								SomeSerial *transparentSerial, uint32_t baudRateTransparent)
{
	CommunicationModule::moduleType = moduleType;
	if (moduleType == COMMMOD_RADIOENGELORAMESH)
	{
		loRaMESHMasterId = 0;
		loRaMESHPayloadSize = payloadSize;
		loRaMESHAckTimeout = ackTimeout;
		loRaMESHAckPayloadSize = 6;
		loRaMESHNumberRetries = numberRetries;

		if (radioEngeLoRaMESH == NULL)
			radioEngeLoRaMESH = new LoRaMESH();
		radioEngeLoRaMESH->InitCommandSerial(commandSerial, baudRateCommand);
		radioEngeLoRaMESH->InitTransparentSerial(transparentSerial, baudRateTransparent);
	}
}

void CommunicationModule::Setup(CommunicationModuleType moduleType,
								uint16_t payloadSize, uint32_t ackTimeout, uint8_t numberRetries,
								uint16_t deviceId,
								SomeSerial *transparentSerial, uint32_t baudRateTransparent)
{
	CommunicationModule::moduleType = moduleType;
	if (moduleType == COMMMOD_RADIOENGELORAMESH)
	{
		loRaMESHMasterId = 0;
		loRaMESHPayloadSize = payloadSize;
		loRaMESHAckTimeout = ackTimeout;
		loRaMESHAckPayloadSize = 6;
		loRaMESHNumberRetries = numberRetries;

		if (radioEngeLoRaMESH == NULL)
			radioEngeLoRaMESH = new LoRaMESH();
		radioEngeLoRaMESH->InitTransparentSerial(transparentSerial, baudRateTransparent);
		radioEngeLoRaMESH->deviceInfo.id = deviceId;
	}
}

void CommunicationModule::SetupLogger(LoggerSerial *loggerSerial)
{
	if (loggerSerial == NULL)
	{
		CommunicationModule::loggerSerial = new LoggerSerial();
	}
	else
		CommunicationModule::loggerSerial = loggerSerial;
}

void CommunicationModule::SendData(uint8_t *data)
{
	SendData(data, loRaMESHPayloadSize);
}

void CommunicationModule::SendData(uint8_t *data, uint16_t data_length)
{
	communicationSuccess = false;
	if (moduleType == COMMMOD_RADIOENGELORAMESH)
	{
		// loggerSerial->println("PREPARING FRAME");
		if (radioEngeLoRaMESH->PrepareFrameTranspBusModule(loRaMESHMasterId, data, data_length) != MESH_OK)
		{
			// loggerSerial->println("PREPARE FRAME TRANSP");
			return;
		}
		if (radioEngeLoRaMESH->SendPacket() != MESH_OK)
		{
			// loggerSerial->println("SEND PACKET");
			return;
		}
		communicationSuccess = true;
	}
}

void CommunicationModule::SendDataWaitAck(uint8_t *data, uint16_t data_length)
{
	communicationSuccess = false;
	if (moduleType == COMMMOD_RADIOENGELORAMESH)
	{
		// for (int c = 0; c < loRaMESHNumberRetries; c++)
		// {
		communicationSuccess = false;
		loggerSerial->println("PREPARING FRAME");
		if (radioEngeLoRaMESH->PrepareFrameTranspBusModule(loRaMESHMasterId, data, data_length) != MESH_OK)
		{
			loggerSerial->println("PREPARE FRAME TRANSP");
		}
		loggerSerial->println("SENDING PACKAGE");
		if (radioEngeLoRaMESH->SendPacket() != MESH_OK)
		{
			loggerSerial->println("SEND PACKET");
		}
		loggerSerial->println("RECEIVING PACKAGE");
		if (radioEngeLoRaMESH->ReceivePacketTransp(loRaMESHAckTimeout) != MESH_OK)
		{
			loggerSerial->println("RECEIVE PACKET ACK");
		}

		uint16_t received_data_length = radioEngeLoRaMESH->frame.size;
		if (received_data_length > 0)
		{
			loggerSerial->println("DATA RECEIVED > 0");
			loggerSerial->println((String)received_data_length);
			uint8_t *received_data = (uint8_t *)malloc(sizeof(uint8_t) * received_data_length);
			loggerSerial->println("MEMCPY TRYING");
			memcpy(received_data, radioEngeLoRaMESH->frame.buffer, received_data_length);

			for (int c = 0; c < received_data_length; c++)
			{
				loggerSerial->print(String(received_data[c], HEX));
			}
			loggerSerial->println(" ");
			loggerSerial->println("MEMCPY NO ERROR");
			if (ValidateAck(received_data, received_data_length) == true)
			{
				communicationSuccess = true;
				loggerSerial->println("COMMUNICATION SUCCESS2");
				if (received_data != NULL)
					free(received_data);
			}
			if (received_data != NULL)
				free(received_data);
		}
		// }
	}
}

bool CommunicationModule::ReceiveData(uint32_t timeout, uint8_t *data, uint16_t *data_length)
{
	bool reply = true;
	receivedDataValid = false;

	if (data_length == NULL)
		data_length = (uint16_t *)malloc(sizeof(uint16_t));
	if (radioEngeLoRaMESH->ReceivePacketTransp(timeout) != MESH_OK)
	{
		reply = false;
		loggerSerial->println("RECEIVE PACKET ACK");
		return reply;
	}
	if (data_length != NULL)
	{
		(*data_length) = radioEngeLoRaMESH->frame.size;
		if (data == NULL)
			data = (uint8_t *)malloc(sizeof(uint8_t) * (*data_length));
		memcpy(data, radioEngeLoRaMESH->frame.buffer, (*data_length));
		uint16_t crc = ((data[(*data_length) - 1] << 8) & 0xFF00) |
					   ((data[(*data_length) - 2] << 0) & 0x00FF);
		if (radioEngeLoRaMESH->deviceInfo.id != 0)
			computed_crc = radioEngeLoRaMESH->ComputeCRCBusModule(data, ((*data_length) - 2));
		else
			computed_crc = radioEngeLoRaMESH->ComputeCRC(data, ((*data_length) - 2));
		if (crc == computed_crc)
			receivedDataValid = true;
	}
	else
	{
		reply = false;
	}
	return reply;
}

bool CommunicationModule::SendAck(uint8_t *data, uint16_t data_length)
{
	if (data_length >= loRaMESHAckPayloadSize)
	{
		uint16_t id = ((data[1] << 8) & 0xFF00) |
					  ((data[0] << 0) & 0x00FF);
		if (radioEngeLoRaMESH->PrepareFrameTranspBusModule(id, &(data[2]),
														   loRaMESHAckPayloadSize) != MESH_OK)
		{
			return false;
			loggerSerial->println("PREPARE FRAME TRANSP");
		}
		if (radioEngeLoRaMESH->SendPacket() != MESH_OK)
		{
			return false;
			loggerSerial->println("SEND PACKET");
		}
		return true;
	}
	else
	{
		return false;
	}
}

bool CommunicationModule::ValidateAck(uint8_t *data, uint16_t data_length)
{
	if (moduleType == COMMMOD_RADIOENGELORAMESH)
	{
		/* Checks if a valid acknowledgement message was received by the Communication Module */
		if (data_length == loRaMESHAckPayloadSize + 2)
		{
			//	0		1		2	3	4	5		6		7		8		9
			// ID_LSB ID_MSB DAY MONTH YEAR HOURS MINUTES SECONDS CRC_LSB CRC_MSB
			uint16_t crc = ((data[data_length - 1] << 8) & 0xFF00) |
						   ((data[data_length - 2] << 0) & 0x00FF);
			uint16_t computed_crc = radioEngeLoRaMESH->ComputeCRCBusModule(data, loRaMESHAckPayloadSize);
			if (crc == computed_crc)
				return true;
			else
				return false;
		}
		else
		{
			return false;
		}
	}
	return true;
}