#ifndef COMMUNICATIONMODULE_H_
#define COMMUNICATIONMODULE_H_

#include <Arduino.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "LoRaMESH/LoRaMESH.h"
#include "LoggerSerial.h"

typedef enum
{
	COMMMOD_RADIOENGELORAMESH,
	COMMMOD_ESP8266,
} CommunicationModuleType;

class CommunicationModule
{
private:
	LoggerSerial *loggerSerial;

	CommunicationModuleType moduleType;

	uint16_t loRaMESHMasterId;
	uint16_t loRaMESHPayloadSize; // payload size in number of bytes
	uint32_t loRaMESHAckTimeout;
	uint16_t loRaMESHAckPayloadSize;
	uint8_t loRaMESHNumberRetries;

public:
	LoRaMESH *radioEngeLoRaMESH;
	bool communicationSuccess;
	bool receivedDataValid;
	uint16_t computed_crc;
	
	CommunicationModule();
	void Setup(CommunicationModuleType moduleType,
			   uint16_t payloadSize, uint32_t ackTimeout, uint8_t numberRetries,
			   SomeSerial *commandSerial, uint32_t baudRateCommand,
			   SomeSerial *transparentSerial, uint32_t baudRateTransparent);
	void Setup(CommunicationModuleType moduleType,
			   uint16_t payloadSize, uint32_t ackTimeout, uint8_t numberRetries,
			   uint16_t deviceId,
			   SomeSerial *transparentSerial, uint32_t baudRateTransparent);
	void SetupLogger(LoggerSerial *loggerSerial);
	void SendData(uint8_t *data);
	void SendData(uint8_t *data, uint16_t data_length);
	void SendDataWaitAck(uint8_t *data, uint16_t data_length);
	bool ReceiveData(uint32_t timeout, uint8_t *data, uint16_t *data_length);
	bool SendAck(uint8_t *data, uint16_t data_length);
	/* Checks if a valid acknowledgement message was received by the Communication Module */
	bool ValidateAck(uint8_t *data, uint16_t data_length);
};

#endif /* COMMUNICATIONMODULE_H_ */